---
title: "Exploring data on mortality, Brazil"
author: "José A Bran - https://ayuda.onecluster.org/"
date: "2021-04-15"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_float: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r }
library(data.table)
library(httr)
library(ggplot2)
library(plotly)
```

World mortality: taking a look to the numbers of the world

We use data collected by the following project:

https://github.com/akarlinsky/world_mortality


```{r }
url = "https://raw.githubusercontent.com/akarlinsky/world_mortality/main/world_mortality.csv"


d <- fread(url)

str(d)
```

```{r }
d[, .N, country_name]
```

```{r }
countries = c("Bolivia", "Brazil", "Chile", "Colombia", "United States")

cw = d[country_name %in% countries  & time_unit == "weekly", ] # not all countries have weekly observations

p = ggplot(cw, aes(year, deaths)) +
    geom_point(aes(col = country_name)) +
    theme_bw() +
    theme(legend.position = "") +
    facet_wrap(~ country_name)

ggplotly(p)
```

```{r }

cm = d[country_name %in% countries  & time_unit == "monthly", ]

p2 = ggplot(cm, aes(year, deaths)) +
    geom_point(aes(col = country_name)) +
    theme_bw() +
    facet_wrap(~ country_name)

ggplotly(p2)
```
